module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        sass: {
			dist: {
				options: {
					precision: 10
				},
				files: {
					'styles/css/app.css': 'styles/scss/app.scss'
				}
			}
        },
        watch: {
			css: {
				files: ['styles/scss/**/*.scss'],
				tasks: ['sass']
            }
        },
        notify_hooks: {
			options: {
				enabled: true,
				max_jshint_notifications: 5,
				title: "SassEmailTemplates",
				success: false,
				duration: 3
			}
		}
    });

    // Load required modules
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-notify');

    // Task definitions
    grunt.registerTask('default', ['notify_hooks', 'watch']);
	grunt.registerTask('compile', ['sass']);
	grunt.registerTask('production', ['sass']);
};
